﻿using System;

namespace WeSellThat.ApiModels
{
    public class SalesModel
    {
        public int CustomerId { get; set; }
        public int OrderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double OrderTotal { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
    }
}
