﻿namespace WeSellThat.ApiModels
{
    public class SearchModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
