﻿using System;

namespace WeSellThat.Domain
{
    public enum OrderStatus
    {
        New = 0,
        InProgress = 1,
        Complete = 2,
        Cancelled = 3
    }

    public class Order
    {
        public int Id { get; set; }
        public double OrderTotal { get; set; }
        public DateTime OrderDate { get; set; }
        public virtual Customer Customer { get; set; }
        public OrderStatus OrderStatus { get; set; }
    }
}
