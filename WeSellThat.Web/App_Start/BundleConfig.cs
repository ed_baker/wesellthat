﻿using System.Web;
using System.Web.Optimization;

namespace WeSellThat.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css", "~/Content/angular-toastr.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include("~/app/app.js", "~/Scripts/angular-toastr.min.js", "~/Scripts/angular-toastr.tpls.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/sales").Include("~/app/Sales/SalesCtrl.js", "~/app/Sales/SalesSrvc.js"));
            bundles.Add(new ScriptBundle("~/bundles/search").Include("~/app/Search/SearchCtrl.js", "~/app/Search/SearchSrvc.js"));
            bundles.Add(new ScriptBundle("~/bundles/customer").Include("~/app/Customer/CustomerCtrl.js", "~/app/Customer/CustomerSrvc.js"));
        }
    }
}
