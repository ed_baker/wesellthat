﻿using System.Web.Http;
using WeSellThat.ApiModels;
using WeSellThat.Core;

namespace WeSellThat.Web.Controllers.Customer
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public IHttpActionResult Get() {
            var result = _customerService.Get();
            return Ok(result);
        }
        
        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get([FromUri] int id) {
            return Ok();
        }
        
        [HttpDelete]
        [Route("delete/{id}")]
        public IHttpActionResult Delete([FromUri] int customerId) {
            _customerService.Delete(customerId);
            return Ok();
        }

        [HttpPut]
        [Route("update/{id}")]
        public IHttpActionResult Put([FromBody] CustomerModel model) {
            _customerService.Update(model);
            return Ok();
        }
    }
}