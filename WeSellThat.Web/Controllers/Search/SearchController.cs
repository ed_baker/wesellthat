﻿using System.Web.Http;
using WeSellThat.ApiModels;
using WeSellThat.Core;

namespace WeSellThat.Web.Controllers.Search
{
    [RoutePrefix("api/search/order")]
    public class SearchController : ApiController
    {
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri] SearchModel search)
        {
            var result = _searchService.Search(search);
            return Ok(result);
        }
    }
}
