﻿using System.Web.Http;
using WeSellThat.Core;

namespace WeSellThat.Web.Controllers.Sales
{
    [RoutePrefix("api/sales")]
    public class SalesController : ApiController
    {
        private readonly ISalesService _salesService;

        public SalesController(ISalesService salesService)
        {
            _salesService = salesService;
        }

        // GET all sales
        [HttpGet]
        public IHttpActionResult Get()
        {
            var result = _salesService.Get();
            return Ok(result);
        }
    }
}
