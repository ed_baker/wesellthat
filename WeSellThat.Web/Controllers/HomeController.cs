﻿using System.Web.Mvc;

namespace WeSellThat.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }

    public class SalesController : Controller
    {
        // GET: Sites
        public ActionResult Index()
        {
            return View();
        }
    }

    public class SearchController : Controller
    {
        // GET: Sites
        public ActionResult Index()
        {
            return View();
        }
    }

    public class CustomerController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
