﻿(function () {
    angular.module('WeSellThat').controller('SearchCtrl', function ($scope, SearchSrvc) {

        $scope.searchObject = { FirstName: "", LastName: "" };
        $scope.searchResults = {};
        $scope.isLoadingData = false;
                
        $scope.search = function () {

            $scope.isLoadingData = true;

            SearchSrvc
                .search($scope.searchObject)
                .success(function (data) {
                    $scope.searchResults = data;
                }).error(function () {
                    console.log("oops");
                });
        
            $scope.isLoadingData = false;
        }

        $scope.reset = function()
        {
            $scope.searchObject.FirstName = "";
            $scope.searchObject.LastName = "";
            $scope.searchResults = {};
        }

        $scope.isButtonActive = function()
        {
            if ($scope.searchObject.FirstName !== "" || $scope.searchObject.LastName !== "") return true;

            return false;
        }
    });
})();