﻿(function () {
    angular.module('WeSellThat').factory('SearchSrvc', ['$http', function ($http) {

        var service = {};
        service.search = function (search) {
            return $http({
                method: 'GET',
                url: '/api/search/orders?firstName='+search.FirstName+'&lastName='+search.LastName
            });
        };
        return service;

    }]);
})();