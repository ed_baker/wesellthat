﻿(function () {
    angular.module('WeSellThat').factory('CustomerSrvc', ['$http', function ($http) {

        var service = {};

        service.getAllCustomers = function () {
            return $http({
                method: 'GET',
                url: '/api/customer'
            });
        };
        service.updateCustomer = function (customer) {
            console.log(customer);
            return $http({
                method: 'PUT',
                url: '/api/customer/update/' + customer.Id,
                data: customer
            });
        };

        return service;

    }]);
})();