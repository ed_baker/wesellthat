﻿(function () {
    angular.module('WeSellThat').controller('CustomerCtrl', function ($scope, CustomerSrvc, toastr) {
        $scope.customers = {};
        $scope.selectedCustomer = null;
        $scope.isLoadingData = false;

        function loadSites() {
            $scope.isLoadingData = true;

            CustomerSrvc
                .getAllCustomers()
                .success(function (data) {
                    $scope.customers = data;
                }).error(function () {
                    console.log("oops");
                });

            $scope.isLoadingData = false;
        }

        $scope.selectCustomer = function (customer) {
            $scope.selectedCustomer = angular.copy(customer, $scope.selectedCustomer);
        }

        $scope.submit = function () {

            CustomerSrvc
                .updateCustomer($scope.selectedCustomer)
                .success(function (data) {
                    toastr.success('Updated customer');
                    $scope.selectedCustomer = null;
                    loadSites();
                })
                .error(function (data) {
                    toastr.success('Error: '+data);
                });
        }

        loadSites();
    });
})();