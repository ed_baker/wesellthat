﻿(function () {
    angular.module('WeSellThat').controller('SalesCtrl', function ($scope, SalesSrvc) {
        $scope.sales = {};
        $scope.sortType = "";
        $scope.sortReverse = false;
        $scope.searchText = [];

        $scope.isLoadingData = false;


        function loadSites() {
            $scope.isLoadingData = true;

            SalesSrvc
                .loadSites()
                .success(function (data) {
                    $scope.sales = data;
                }).error(function () {
                    console.log("oops");
                });

            $scope.isLoadingData = false;
        }
        loadSites();
    });
})();