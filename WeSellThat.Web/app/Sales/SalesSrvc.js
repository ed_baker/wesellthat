﻿(function () {
    angular.module('WeSellThat').factory('SalesSrvc', ['$http', function ($http) {

        var service = {};
        service.loadSites = function () {
            return $http({
                method: 'GET',
                url: '/api/sales'
            });
        };
        return service;

    }]);
})();