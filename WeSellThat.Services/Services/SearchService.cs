﻿using System;
using System.Collections.Generic;
using System.Linq;
using WeSellThat.ApiModels;
using WeSellThat.Core;
using WeSellThat.Services.Persistence;

namespace WeSellThat.Services.Services
{
    public class SearchService : ISearchService
    {
        public List<SalesModel> Search(SearchModel search)
        {
            var result = new List<SalesModel>();

            using (var context = new CandidateTestingContext())
            {
                result = context.Orders.Where(o => o.Customer.FirstName == search.FirstName || o.Customer.LastName == search.LastName).Select(i => new SalesModel
                {
                    OrderId = i.Id,
                    CustomerId = i.Customer.Id,
                    FirstName = i.Customer.FirstName,
                    LastName = i.Customer.LastName,
                    OrderDate = i.OrderDate,
                    OrderTotal = i.OrderTotal,
                    OrderStatus = i.OrderStatus.ToString()
                }).ToList();
            }

            return result;
        }
    }
}
