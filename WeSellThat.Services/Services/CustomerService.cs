﻿using System;
using System.Collections.Generic;
using System.Linq;
using WeSellThat.ApiModels;
using WeSellThat.Core;
using WeSellThat.Services.Persistence;

namespace WeSellThat.Services.Services
{
    public class CustomerService : ICustomerService
    {
        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public List<CustomerModel> Get()
        {
            var result = new List<CustomerModel>();

            using (var context = new CandidateTestingContext())
            {
                result = context.Customers.Select(i => new CustomerModel
                {
                    Id = i.Id,
                    FirstName = i.FirstName,
                    LastName = i.LastName,
                    Address = i.Address,
                    City = i.City,
                    State = i.State,
                    Zipcode = i.Zipcode
                }).ToList();
            }

            return result;
        }

        public CustomerModel Get(int customerId)
        {
            throw new NotImplementedException();
        }

        public void Update(CustomerModel customer)
        {
            using (var context = new CandidateTestingContext())
            {
                var customerRecord = context.Customers.FirstOrDefault(i => i.Id == customer.Id);

                if (customerRecord == null) throw new ApplicationException();

                customerRecord.FirstName = customer.FirstName;
                customerRecord.LastName = customer.LastName;
                customerRecord.Address = customer.Address;
                customerRecord.City = customer.City;
                customerRecord.State = customer.State;
                customerRecord.Zipcode = customer.Zipcode;

                context.SaveChanges();
            }
        }
    }
}
