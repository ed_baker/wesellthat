namespace WeSellThat.Services.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Collections.Generic;
    using Domain;

    internal sealed class Configuration : DbMigrationsConfiguration<Persistence.CandidateTestingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Persistence.CandidateTestingContext context)
        {
            // initial seed stuff (from example excel file)
            context.Customers.AddOrUpdate(i => new { i.FirstName, i.LastName },
                new Customer
                {
                    Id = 8790,
                    FirstName = "Robert",
                    LastName = "Powers",
                    Orders = new List<Order>
                    {
                        new Order { Id = 1023, OrderTotal = 19.99, OrderDate = new System.DateTime(2014, 12, 02), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1024, OrderTotal = 18.01, OrderDate = new System.DateTime(2014, 12, 06), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1029, OrderTotal = 44.54, OrderDate = new System.DateTime(2015, 01, 02), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1037, OrderTotal = 23.11, OrderDate = new System.DateTime(2015, 02, 27), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1043, OrderTotal = 123.54, OrderDate = new System.DateTime(2015, 03, 26), OrderStatus = OrderStatus.Cancelled },
                        new Order { Id = 1044, OrderTotal = 3.33, OrderDate = new System.DateTime(2015, 03, 26), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1045, OrderTotal = 65.66, OrderDate = new System.DateTime(2015, 03, 28), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1047, OrderTotal = 22.15, OrderDate = new System.DateTime(2015, 04, 02), OrderStatus = OrderStatus.InProgress },
                    }
                },
                new Customer
                {
                    Id = 7860,
                    FirstName = "Alex",
                    LastName = "Scott",
                    Orders = new List<Order>()
                    {
                        new Order { Id = 1025, OrderTotal = 17.45, OrderDate = new System.DateTime(2014, 12, 20), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1028, OrderTotal = 23.56, OrderDate = new System.DateTime(2015, 01, 02), OrderStatus = OrderStatus.Cancelled },
                        new Order { Id = 1032, OrderTotal = 8.98, OrderDate = new System.DateTime(2015, 01, 05), OrderStatus = OrderStatus.Complete },
                    }
                },
                new Customer
                {
                    Id = 1228,
                    FirstName = "Cody",
                    LastName = "Smith",
                    Orders = new List<Order>()
                    {
                        new Order { Id = 1026, OrderTotal = 53.77, OrderDate = new System.DateTime(2014, 12, 28), OrderStatus = OrderStatus.New },
                    }
                },
                new Customer
                {
                    Id = 1876,
                    FirstName = "Gavin",
                    LastName = "Bowers",
                    Orders = new List<Order>()
                    {
                        new Order { Id = 1027, OrderTotal = 12.54, OrderDate = new System.DateTime(2015, 01, 02), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1030, OrderTotal = 2.13, OrderDate = new System.DateTime(2015, 01, 03), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1034, OrderTotal = 16.54, OrderDate = new System.DateTime(2015, 01, 16), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1041, OrderTotal = 3.44, OrderDate = new System.DateTime(2015, 03, 16), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1042, OrderTotal = 67.76, OrderDate = new System.DateTime(2015, 03, 18), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1049, OrderTotal = 23.22, OrderDate = new System.DateTime(2015, 04, 02), OrderStatus = OrderStatus.Complete },
                    }
                },
                new Customer
                {
                    Id = 2433,
                    FirstName = "Ashley",
                    LastName = "Anderson",
                    Orders = new List<Order>()
                    {
                        new Order { Id = 1031, OrderTotal = 1.95, OrderDate = new System.DateTime(2015, 01, 04), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1033, OrderTotal = 101.54, OrderDate = new System.DateTime(2015, 01, 12), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1035, OrderTotal = 84.91, OrderDate = new System.DateTime(2015, 01, 28), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1036, OrderTotal = 92.45, OrderDate = new System.DateTime(2015, 02, 06), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1040, OrderTotal = 8.00, OrderDate = new System.DateTime(2015, 03, 16), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1046, OrderTotal = 23.32, OrderDate = new System.DateTime(2015, 03, 28), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1050, OrderTotal = 5.23, OrderDate = new System.DateTime(2015, 04, 18), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1051, OrderTotal = 9.76, OrderDate = new System.DateTime(2015, 05, 02), OrderStatus = OrderStatus.Complete },
                    }
                },
                new Customer
                {
                    Id = 7765,
                    FirstName = "Jen",
                    LastName = "Gunderson",
                    Orders = new List<Order>()
                    {
                        new Order { Id = 1038, OrderTotal = 4.46, OrderDate = new System.DateTime(2015, 03, 02), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1039, OrderTotal = 7.56, OrderDate = new System.DateTime(2015, 03, 06), OrderStatus = OrderStatus.Complete },
                        new Order { Id = 1048, OrderTotal = 23.32, OrderDate = new System.DateTime(2015, 04, 02), OrderStatus = OrderStatus.Complete },
                    }
                }
                );
            

        }
    }
}
