﻿using System.Data.Entity;
using WeSellThat.Domain;

namespace WeSellThat.Services.Persistence
{
    public class CandidateTestingContext : DbContext
    {
        public CandidateTestingContext() : base("CandidateTesting") { }

        public IDbSet<Customer> Customers { get; set; }
        public IDbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasMany(o => o.Orders);
        }

    }
}
