﻿using System.Collections.Generic;
using WeSellThat.ApiModels;

namespace WeSellThat.Core
{
    public interface ISearchService
    {
        List<SalesModel> Search(SearchModel search);
    }
}
