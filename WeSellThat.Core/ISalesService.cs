﻿using System.Collections.Generic;
using WeSellThat.ApiModels;

namespace WeSellThat.Core
{
    public interface ISalesService
    {
        List<SalesModel> Get();
    }
}
