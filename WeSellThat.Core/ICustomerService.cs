﻿using System.Collections.Generic;
using WeSellThat.ApiModels;

namespace WeSellThat.Core
{
    public interface ICustomerService
    {
        CustomerModel Get(int customerId);
        List<CustomerModel> Get();
        void Update(CustomerModel Id);
        void Delete(int Id);
    }
}
